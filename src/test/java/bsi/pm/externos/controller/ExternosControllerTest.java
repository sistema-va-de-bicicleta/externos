package bsi.pm.externos.controller;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import cartaodecredito.CartaoDeCredito;
import cobranca.Cobranca;
import email.Email;


@SpringBootTest
@AutoConfigureMockMvc
public class ExternosControllerTest {

	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private MockMvc mockMvc;

	 @Test
	 void cobrancaTest() throws Exception {
	    	Cobranca cobranca = new Cobranca("001122", 10);
	    	cobranca.setHoraSolicitacao("2022-01-14T20:36:00.491Z");
	    	
		    String cobrancaJSON = objectMapper.writeValueAsString(cobranca);
			this.mockMvc.perform(post("/cobranca/").contentType(MediaType.APPLICATION_JSON).content(cobrancaJSON)).andExpect(status().is(200))
					.andExpect(content().string(containsString("001122")));
	 }
	 
	@Test
	void getCobrancaByIdTest() throws Exception {
		this.mockMvc.perform(get("/cobranca/3fa85f64-5717-4562-b3fc-2c963f66afa6")).andExpect(status().is(200))
				.andExpect(content().string(containsString("3fa85f64-5717-4562-b3fc-2c963f66afa6")));
    }
	
	 @Test
	 void filaCobrancaTest() throws Exception {
	    	Cobranca cobranca = new Cobranca("001122", 10);
	    	cobranca.setHoraSolicitacao("2022-01-14T20:36:00.491Z");
	    	
		    String cobrancaJSON = objectMapper.writeValueAsString(cobranca);
			this.mockMvc.perform(post("/filaCobranca/").contentType(MediaType.APPLICATION_JSON).content(cobrancaJSON)).andExpect(status().is(200))
					.andExpect(content().string(containsString("001122")));
	 }
	
	 @Test
	 void emailTest() throws Exception {
		 	Email email = new Email("arianne.carneiro@uniriotec.br", "Teste Externos", "Teste realizado!");
		    String emailJSON = objectMapper.writeValueAsString(email);
			this.mockMvc.perform(post("/enviarEmail/").contentType(MediaType.APPLICATION_JSON).content(emailJSON)).andExpect(status().is(200))
					.andExpect(content().string(containsString("arianne.carneiro@uniriotec.br")));
	 }
	 
	 @Test
	 void validaCartaoDeCreditoTest() throws Exception {
		 	CartaoDeCredito cartao = new CartaoDeCredito(
	                "4024007153763191", "646", "02/2024", null);
		 	objectMapper.setSerializationInclusion(Include.NON_NULL);
		    String cartaoJSON = objectMapper.writeValueAsString(cartao);
			this.mockMvc.perform(post("/validaCartaoDeCredito/").contentType(MediaType.APPLICATION_JSON).content(cartaoJSON)).andExpect(status().is(200))
					.andExpect(content().string(containsString("Dados atualizados.")));
	 }

}

