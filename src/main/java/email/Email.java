package email;

import java.security.SecureRandom;

public class Email {
	private String id;
	private String email;
	private String mensagem;
	private String titulo;
	
	public Email() {
		
	}

	public Email(String email, String mensagem, String titulo) {
		SecureRandom rand = new SecureRandom();
		this.id = rand.nextInt(1000)+"";
		this.email = email;
		this.mensagem = mensagem;
		this.titulo = titulo;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
