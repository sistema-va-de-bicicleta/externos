package bsi.pm.externos.service;

import java.security.GeneralSecurityException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;

import com.sun.mail.util.MailSSLSocketFactory;

import erro.Erro;

@Service
public class SendEmailService {

    // Assuming you are sending email from through gmails smtp
    String host = "smtp.gmail.com";
    
    // Sender's email ID needs to be mentioned
    String from = "sistemavadebicicleta@gmail.com";
    
	// Get system properties
    Properties properties = System.getProperties();

	public SendEmailService() throws GeneralSecurityException {
		
		MailSSLSocketFactory sf = new MailSSLSocketFactory();
		sf.setTrustAllHosts(true); 
		properties.put("mail.smtp.ssl.trust", "*");
		properties.put("mail.smtp.ssl.socketFactory", sf);	
	    properties.put("mail.smtp.host", host);
	    properties.put("mail.smtp.port", "465");
	    properties.put("mail.smtp.ssl.enable", "true");
	    properties.put("mail.smtp.auth", "true");

	}
	
	public Erro send(String to, String emailSubject, String emailText) {
		
        // Get the Session object.// and pass username and password
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
        	@Override
            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication("sistemavadebicicleta@gmail.com", "cbfkeosojucdtima");

            }

        });

        // Used to debug SMTP issues
        session.setDebug(true);
		
        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject(emailSubject);

            // Now set the actual message
            message.setText(emailText);

            System.out.println("sending...");
            // Send message
            Transport.send(message, "sistemavadebicicleta@gmail.com", "cbfkeosojucdtima");
            System.out.println("Sent message successfully....");
            return null;
        } catch (AddressException e) {
			Erro erro = new Erro();
			erro.setCodigo("404");
			erro.setMensagem("E-mail não existe");
			return erro;
		} catch (MessagingException e) {
			Erro erro = new Erro();
			erro.setCodigo("422");
			erro.setMensagem("E-mail com formato inválido");
			return erro;
		}

		
	}
	
	

}

