package bsi.pm.externos.service;

import org.springframework.stereotype.Service;

import cobranca.Cobranca;
import cobranca.FilaCobranca;
import cobranca.StatusCobranca;

@Service
public class FilaCobrancaService {

	public FilaCobranca getFilaCobrancaPreenchida() {
		FilaCobranca filaCobrancas = new FilaCobranca();

		filaCobrancas.getFilaCobranca()
				.add(new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa7", StatusCobranca.PENDENTE,
						"2022-01-14T20:33:00.491Z", "2022-01-14T20:33:00.491Z", 1,
						"3fa85f64-5717-4562-b3fc-2c963f66afa9"));

		filaCobrancas.getFilaCobranca().add(new Cobranca("3fa85f62-5717-4562-b3fc-2c963f66afa8",
				StatusCobranca.CANCELADA, "2022-01-15T20:35:00.491Z", "", 15, "124"));

		filaCobrancas.getFilaCobranca().add(new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa6", StatusCobranca.PAGA,
				"2022-01-14T20:36:00.491Z", "2022-01-14T20:37:00.491Z", 5, "532"));

		return filaCobrancas;
	}

	public FilaCobranca getFilaCobranca() {
		return new FilaCobranca();
	}

}
