package bsi.pm.externos.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;


import cartaodecredito.CartaoDeCredito;
import cobranca.Cobranca;
import cobranca.StatusCobranca;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

@Service
public class CobrancaService {

	private static final String URL_ALUGUEL = "https://aluguel-vadebicicleta.herokuapp.com";

	public CartaoDeCredito recuperaCartao(String ciclistaId) {
		HttpResponse<CartaoDeCredito> response = Unirest.get(URL_ALUGUEL + "/cartaoDeCredito/" + ciclistaId)
				.header("Content-Type", "application/json").asObject(CartaoDeCredito.class);
		return response.getBody();
	}

	public ArrayList<Cobranca> getCobrancas() {
		ArrayList<Cobranca> cobrancas = new ArrayList<Cobranca>();
		cobrancas.add(new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa7", 1));

		cobrancas.add(new Cobranca("3fa85f64-5717-4562-b3fc-2c963f66afa6", StatusCobranca.PENDENTE, "2022-01-13T23:58:44.150Z",
				"2022-01-13T23:58:44.150Z", 1, "3fa85f64-5717-4562-b3fc-2c963f66afa8"));

		return cobrancas;
	}

	public Cobranca getCobrancaById(String id) {
		ArrayList<Cobranca> cobrancas = getCobrancas();

		try {
			for (int i = 0; i < cobrancas.size(); i++) {
				if (cobrancas.get(i).getId().equals(id)) {
					return cobrancas.get(i);
				}
			}
		} catch (NullPointerException n) {
			return null;
		}

		return null;
	}

}
