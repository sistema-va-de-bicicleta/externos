package bsi.pm.externos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
public class ExternosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExternosApplication.class, args); 
	}

	@Bean
	public ObjectMapper buildObjectMapper() {
		return new ObjectMapper();
	}
}
