package bsi.pm.externos.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import bsi.pm.externos.service.FilaCobrancaService;
import cobranca.Cobranca;
import cobranca.FilaCobranca;
import erro.Erro;

@RestController
@RequestMapping("/filaCobranca")
public class FilaCobrancaController {

	@Autowired
	private FilaCobrancaService filaCobrancaService;
	
	@PostMapping("/")
	public ResponseEntity<Object> filaCobranca (@RequestBody Cobranca cobranca)
	{
    	
		try {

	    	FilaCobranca fila = this.filaCobrancaService.getFilaCobrancaPreenchida();
	    	fila.addFilaCobranca(cobranca);	
	    	return ResponseEntity.status(200).body(cobranca);

		} catch(Exception ex) {
			Erro erro = new Erro();
			erro.setCodigo("422");
			erro.setMensagem("Dados invalidos.");
			return ResponseEntity.status(Integer.parseInt(erro.getCodigo())).body(erro);
		}
		
    }

	
}

