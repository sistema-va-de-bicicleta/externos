package bsi.pm.externos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import bsi.pm.externos.service.CobrancaService;
import cartaodecredito.CartaoDeCredito;
import cobranca.Cobranca;
import cobranca.GeracaoCobranca;
import erro.Erro;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;


@RestController
@RequestMapping("/cobranca")
public class CobrancaController {

	@Autowired
	private CobrancaService cobrancaService;
	
	@GetMapping("/{idCobranca}")
	public ResponseEntity<Object> cobranca(@PathVariable(required = true) String idCobranca) {

		Cobranca cobranca = this.cobrancaService.getCobrancaById(idCobranca);

		if (cobranca != null) {
			return ResponseEntity.status(200).body(cobranca);
		}

		Erro erro = new Erro();
		erro.setCodigo("404");
		erro.setMensagem("Cobrança não encontrada.");
		return ResponseEntity.status(Integer.parseInt(erro.getCodigo())).body(erro);
	}
	
	@PostMapping("/")
	public ResponseEntity<Object> cobranca(@RequestBody Cobranca cobranca) {
		CartaoDeCredito cartao = this.cobrancaService.recuperaCartao(cobranca.getCiclista());

		GeracaoCobranca expedidora = new GeracaoCobranca(cobranca, cartao);

		HttpResponse<JsonNode> response = expedidora.enviaCobranca();

		if (response.getStatus() == 201) {
			return ResponseEntity.status(200).body(cobranca);
		}

		Erro erro = new Erro();
		erro.setCodigo("422");
		erro.setMensagem("Dados invalidos.");
		return ResponseEntity.status(Integer.parseInt(erro.getCodigo())).body(erro);
	}

}

