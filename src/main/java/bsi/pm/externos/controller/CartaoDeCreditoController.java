package bsi.pm.externos.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cartaodecredito.CartaoDeCredito;
import cobranca.GeracaoCobranca;
import erro.Erro;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;

@RestController
@RequestMapping("/validaCartaoDeCredito")
public class CartaoDeCreditoController {
	
	@PostMapping("/")
	public ResponseEntity<Object> validaCartaoCredito(@RequestBody CartaoDeCredito cartao) {

		GeracaoCobranca expedidora = new GeracaoCobranca();
		HttpResponse<JsonNode> response = expedidora.validaCartaoDeCredito(cartao);
		
		if (response.getStatus() == 201) {
			return ResponseEntity.status(200).body("Dados atualizados.");
		} else if (response.getStatus() == 422) {
			Erro erro = new Erro();
			erro.setCodigo("422");
			erro.setMensagem("Dados invalidos.");
			return ResponseEntity.status(Integer.parseInt(erro.getCodigo())).body(erro);
			
		}
		
		Erro erro = new Erro();
		erro.setCodigo("402");
		return ResponseEntity.status(Integer.parseInt(erro.getCodigo())).body(erro);

	};

}
