package bsi.pm.externos.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import bsi.pm.externos.service.SendEmailService;
import email.Email;
import erro.Erro;

@RestController
@RequestMapping("/enviarEmail")
public class EmailController {

	@Autowired
	private SendEmailService sendEmailService;
	
	@PostMapping("/")
	public ResponseEntity<Object> enviarEmail(@RequestBody Email email) {
		// Método para enviar email

    	Erro erro = sendEmailService.send(email.getEmail(), email.getTitulo(), email.getMensagem());
		if(Objects.isNull(erro))
			return ResponseEntity.status(200).body(email);
		
		return ResponseEntity.status(Integer.parseInt(erro.getCodigo())).body(erro);
		
	}
	
}
