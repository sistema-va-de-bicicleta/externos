package cartaodecredito;


public class CartaoDeCredito {
	
	private String id;
	private String numero;
	private String cvv;
	private String validade;
	private String nomeTitular;
	
	
	public CartaoDeCredito(String numero, String cvv, String validade, String nomeTitular) {
		this.numero = numero;
		this.cvv = cvv;
		this.validade = validade;
		this.nomeTitular = nomeTitular;
	}
	
	public CartaoDeCredito() {

	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNumero() {
		return this.numero;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public String getCvv() {
		return this.cvv;
	}
	
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	
	public String getValidade() {
		return this.validade;
	}
	
	public void setValidade(String validade) {
		this.validade = validade;
	}
	
	public String getNomeTitular() {
		return this.nomeTitular;
	}
	
	public void setNomeTitular(String nomeTitular) {
		this.nomeTitular = nomeTitular;
	}
	
}
