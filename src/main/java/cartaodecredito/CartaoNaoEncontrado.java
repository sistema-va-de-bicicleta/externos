package cartaodecredito;

public class CartaoNaoEncontrado extends Exception {
	public CartaoNaoEncontrado(String mensagem) {
		super(mensagem);
	}
}
