package cobranca;

public enum StatusCobranca {

	PENDENTE, PAGA, FALHA, CANCELADA, OCUPADA;
}
