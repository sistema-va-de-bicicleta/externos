package cobranca;

import java.util.UUID;

public class Cobranca {
	private String id;
	private StatusCobranca status;
	private String horaSolicitacao;
	private String horaFinalizacao;
	private int valor;
	private String ciclista;
	
	public Cobranca(String ciclista, int valor) {
		this.id = UUID.randomUUID().toString();
		this.valor = valor;
		this.ciclista = ciclista;
	}
	
	public Cobranca(String id, StatusCobranca status, String horaSolicitacao, String horaFinalizacao, int valor, String ciclista) {
		this.id = id;
		this.status = status;
		this.horaSolicitacao = horaSolicitacao;
		this.horaFinalizacao = horaFinalizacao;
		this.valor = valor;
		this.ciclista = ciclista;
	}
	
	public Cobranca(StatusCobranca status, String horaSolicitacao, String horaFinalizacao, int valor, String ciclistaId) {
		this.id = UUID.randomUUID().toString();
		this.status = status;
		this.horaSolicitacao = horaSolicitacao;
		this.horaFinalizacao = horaFinalizacao;
		this.valor = valor;
		this.ciclista = ciclistaId;
	}
	
	public Cobranca() {

	}
	
	public String getId() {
		return id;
	}

	public String getHoraSolicitacao() {
		return horaSolicitacao;
	}

	public void setHoraSolicitacao(String horaSolicitacao) {
		this.horaSolicitacao = horaSolicitacao;
	}

	public String getHoraFinalizacao() {
		return horaFinalizacao;
	}

	public void setHoraFinalizacao(String horaFinalizacao) {
		this.horaFinalizacao = horaFinalizacao;
	}

	public String getCiclista() {
		return ciclista;
	}

	public void setCiclista(String ciclista) {
		this.ciclista = ciclista;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public StatusCobranca getStatus() {
		return status;
	}

	public void setStatus(StatusCobranca status) {
		this.status = status;
	}
}
