package cobranca;

import java.util.Queue;
import java.util.LinkedList;

public class FilaCobranca {
	
	private Queue<Cobranca> filaDeCobrancas = new LinkedList<>();
	
	public FilaCobranca() {
		
	}

	public FilaCobranca(Queue<Cobranca> filaCobranca) {
		for (Cobranca cobranca : filaCobranca) {
			this.filaDeCobrancas.add(cobranca);
		}
		
	}
	
	public Queue<Cobranca> getFilaCobranca() {
		return filaDeCobrancas;
	}

	public void addFilaCobranca(Cobranca cobranca) {
		this.filaDeCobrancas.add(cobranca);
	}

}
