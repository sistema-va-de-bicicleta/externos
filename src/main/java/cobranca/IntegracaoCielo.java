package cobranca;

public class IntegracaoCielo {
	
	public String MerchantOrderId;
	public Customer Customer;
	public Pagamento Payment;
	
	public IntegracaoCielo(String MerchantOrderId) {
		this.Customer = new Customer();
		this.MerchantOrderId = MerchantOrderId;
		this.Payment = new Pagamento();
	}
	
	public class Customer {
	      public String Name;
	}

}
